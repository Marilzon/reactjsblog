# ReactJSBLOG

Blog desenvolvido em ReactJS

## Recursos:

 - Uma página principal que liste as postagens pelo mais recente (sem paginação), com número de likes; Foto do autor; Categoria; E opção de abrir detalhes.
 - Uma página de detalhes de postagem, mostrando o corpo da postagem; Todos os comentários ordenados por mais recente; Categoria; Detalhes do autor;

Critérios de avaliação:
