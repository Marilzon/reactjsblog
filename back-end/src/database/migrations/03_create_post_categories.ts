import Knex from 'knex';

export async function up(knex: Knex) {
  return knex.schema.createTable('post_categories', table => {
    table.increments('id').primary();

    table.integer('post_id')
      .references('id')
      .inTable('posts');

    table.integer('category_id')
      .references('id')
      .inTable('categories');
  });
}
export async function down(knex: Knex) {
  return knex.schema.dropTable('post_categories');
}
