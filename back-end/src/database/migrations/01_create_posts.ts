import Knex from 'knex';

export async function up(knex: Knex) {
  return knex.schema.createTable('posts', table => {
    table.increments('id').primary();
    table.string('title', 45);
    table.string('content');
    table.string('postedBy');
    table.timestamp('postedAt');
    table.integer('likes');
  });
}
export async function down(knex: Knex) {
  return knex.schema.dropTable('posts');
}
