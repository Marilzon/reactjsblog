import Knex from 'knex';

export async function seed(knex: Knex) {
  await knex('categories').insert([
    { name: 'Educação', color: '#3a3a3a' },
    { name: 'Financias', color: '#3a3a3a' },
    { name: 'Técnologia', color: '#3a3a3a' },
    { name: 'Saúde', color: '#3a3a3a' },
    { name: 'Esportes', color: '#3a3a3a' },
    { name: 'Entretenimento', color: '#3a3a3a' },
    { name: 'Games', color: '#3a3a3a' },
    { name: 'Animais', color: '#3a3a3a' },
    { name: 'Outros', color: '#3a3a3a' },
  ]);
}
