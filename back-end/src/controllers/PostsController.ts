import { Request, Response } from 'express';
import knex from '../database/connection';

class PostsController {
  async index(request: Request, response: Response) {

    const posts = await knex.select('*').from('posts').orderBy('id', 'desc');

    if (!posts) {
      return response.status(400).json({ message: 'Não existem posts' });
    }

    return response.send(posts);
  }

  async show(request: Request, response: Response) {
    const { id } = request.params;

    const post = await knex('posts').where('id', id).first();

    if (!post) {
      return response.status(400).json({ message: 'Post não encontrado!' });
    }

    const categories = await knex('categories')
      .join('post_categories', 'categories.id', '=', 'post_categories.category_id')
      .where('post_categories.post_id', id)
      .select('categories.name');

    return response.json({ post, categories });
  }

  async create(request: Request, response: Response) {
    const trx = await knex.transaction();

    const {
      title,
      content,
      postedBy,
      postedAt,
      categories,
      likes,
    } = request.body;

    const post = {
      title,
      content,
      postedBy,
      postedAt,
      likes,
    }

    const insertedIds = await trx('posts').insert(post);

    const post_id = insertedIds[0];

    const postCategories = categories.map((category_id: number) => {
      return {
        category_id,
        post_id,
      }
    });

    await trx('post_categories').insert(postCategories);

    await trx.commit();

    return response.json({
      id: post_id,
      ...post
    });
  }
}

export default PostsController;
