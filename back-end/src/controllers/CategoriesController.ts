import { Request, Response } from 'express';
import knex from '../database/connection';

class CategoriesController {
  async index(request: Request, response: Response) {
    const categories = await knex('categories').select('*');

    const listCategories = categories.map(category => {
      return {
        id: category.id,
        title: category.name,
        color: category.color,
      };
    });

    return response.json(listCategories);
  }
}

export default CategoriesController;
