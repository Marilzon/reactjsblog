import express from 'express';

import PostsController from './controllers/PostsController';
import CategoriesController from './controllers/CategoriesController';

const routes = express.Router();
const postsController = new PostsController;
const categoriesController = new CategoriesController;

routes.get('/categories', categoriesController.index);

routes.post('/posts', postsController.create);
routes.get('/posts', postsController.index);
routes.get('/posts/:id', postsController.show);

export default routes;
