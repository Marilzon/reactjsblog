import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Dashboard from '../pages/Dashboard';
import CreatePost from '../pages/Posts/CreatePost';
import Posts from '../pages/Posts/IndexPosts';

const Routes: React.FC = () => (
  <Switch>
    <Route path="/" exact component={Dashboard} />
    <Route path="/create" component={CreatePost} />
    <Route path="/posts" component={Posts} />
    <Route path="/posts/:id" component={Posts} />
  </Switch>
);

export default Routes;
