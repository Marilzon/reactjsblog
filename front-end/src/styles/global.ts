import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  body {
    -webkit-font-smoothing: antialiased;
    background-color: #F2F2F2;
  }

  #root {
    max-width: 1480px;
    margin: 0 auto;
    padding: 20px 10px;
    height: 100vh;
  }

  button {
    cursor: pointer;
  }
`;
