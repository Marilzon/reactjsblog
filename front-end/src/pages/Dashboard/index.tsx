import React from 'react';
import { Link } from 'react-router-dom';

import { Button } from '../../components/Buttons/styles';
import Posts from '../Posts/IndexPosts';

import { Container } from './styles';

const Dashboard: React.FC = () => {
  return (
    <>
      <Container>
        <Button as={Link} to="/create">Criar novo post</Button>
      </Container>
      <Container>
        <Posts />
      </Container>

    </>
  );
}

export default Dashboard;
