import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  justify-content: center;
  color: #3a3a3a;
`;

