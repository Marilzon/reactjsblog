import styled from 'styled-components';

export const Form = styled.form`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;

  padding: 12px;
  border: 1px solid #9FA3A6;
`;

export const FieldGroup = styled.div`
  display: flex;
  flex-flow: column;
  width: 480px;
  padding: 2%;

  input[type=text], select, textarea {
    width: 100%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    resize: vertical;
  }
`;
