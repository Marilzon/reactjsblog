
import React, { ChangeEvent, FormEvent, useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';

import api from '../../../services/api';

import { Button } from '../../../components/Buttons/styles';
import { FieldGroup, Form } from './styles';

interface Category {
  id: number;
  title: string;
  color: string;
}

const CreatePost: React.FC = () => {
  const [categories, setCategories] = useState<Category[]>([]);

  const [inputData, setInputData] = useState({
    title: '',
    content: '',
    postedBy: '',
    postedAt: Date.now(),
    likes: 0
  });

  const [selectedCategories, setSelectedCategories] = useState<number[]>([]);

  const history = useHistory();

  function handleInputTitle(event: ChangeEvent<HTMLInputElement>) {
    const { name, value } = event.target;

    setInputData({ ...inputData, [name]: value, });
  }

  function handleInputContent(event: ChangeEvent<HTMLTextAreaElement>) {
    const { name, value } = event.target;

    setInputData({ ...inputData, [name]: value, });
  }

  function handleSelectItem(id: number) {
    const alreadySelected = selectedCategories.findIndex(category => category === id);

    if (alreadySelected >= 0) {
      const filteredCategories = selectedCategories.filter(category => category !== id)

      setSelectedCategories(filteredCategories);
    } else {
      setSelectedCategories([...selectedCategories, id]);
    }
  }

  async function handleSubmit(event: FormEvent) {
    event.preventDefault();

    const { title, content, likes } = inputData;

    const categories = selectedCategories;

    const data = {
      title,
      content,
      categories,
      postedAt: Date.now(),
      likes,
    }

    await api.post('posts', data);

    alert('Post cadastrado!');

    history.push('/');
  }

  useEffect(() => {
    api.get('categories').then(response => {
      setCategories(response.data);
    })
  }, []);

  return (
    <Form onSubmit={handleSubmit}>
      <h1>Criar Post</h1>
      <FieldGroup>
        <label htmlFor='title'>Título do post</label>
        <input
          type='text'
          name='title'
          id='title'
          onChange={handleInputTitle}
          minLength={3}
          maxLength={25}
          autoComplete='off'
          required={true}
        />
      </FieldGroup>

      <FieldGroup>
        <label htmlFor='content'>Conteudo</label>
        <textarea
          name='content'
          id='content'
          onChange={handleInputContent}
          minLength={10}
          autoComplete='off'
          required={true}
        />
      </FieldGroup>

      <FieldGroup>
        <label htmlFor='categories'>Categorias</label>

        <select
          multiple
          required={true}
          className='category-options'
        >
          {categories.map(category => (
            <option
              key={category.id}
              onClick={() => handleSelectItem(category.id)}
              className={selectedCategories.includes(category.id) ? 'category_selected' : ''
              }
            >
              {category.title}
            </option>
          ))}
        </select>
        <Button type='submit'>Postar</Button>
      </FieldGroup>
      <Button as={Link} to='/'>Voltar</Button>
    </Form>
  );
}

export default CreatePost;

