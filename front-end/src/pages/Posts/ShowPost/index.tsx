import React, { useEffect, useState } from 'react';

import api from '../../../services/api';

import { Button } from '../../../components/Buttons/styles';

interface PostTypes {
  id: number;
  title: string;
  content: string;
  categories: string;
  postedBy: string;
  postedAt: number;
  likes: number;
}

const Posts: React.FC = () => {
  const [posts, setPosts] = useState<PostTypes[]>([]);

  useEffect(() => {
    api.get('posts').then(response => {
      setPosts(response.data);
    })
  }, []);

  return (
    <div>
      {posts.map(post => (
        <div key={post.id}>
          <h2>{post.title}</h2>
          <p>Data da postagem: {post.postedAt}</p>
          <p>Autor: {post.postedBy}</p>
          <p>Likes: {post.likes}</p>
          Categoria: <span>Nome Categoria </span>
          <div>
            {post.content}
          </div>
        </div>
      ))}
    </div>
  );
}

export default Posts;
