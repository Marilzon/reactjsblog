import React, { useEffect, useState } from 'react';

import api from '../../../services/api';

import { Button } from '../../../components/Buttons/styles';
import { PostContainer, Container } from './styles';
import { Link } from 'react-router-dom';

interface PostTypes {
  id: number;
  title: string;
  content: string;
  categories: string;
  postedBy: string;
  postedAt: number;
  likes: number;
}

const Posts: React.FC = () => {
  const [posts, setPosts] = useState<PostTypes[]>([]);

  useEffect(() => {
    api.get('posts').then(response => {
      setPosts(response.data);
    })
  }, []);

  return (
    <Container>
      {posts.map(post => (
        <PostContainer key={post.id}>
          <h2>{post.title}</h2>
          <p>Data da postagem: {post.postedAt}</p>
          <p>Autor: {post.postedBy}</p>
          <p>Likes: {post.likes}</p>
          Categoria: <span>Nome Categoria </span>
          <br /><br />
          <Button as={Link} type='button' to='/posts/:id'>Ver Mais...</Button>
        </PostContainer>
      ))}
    </Container>
  );
}

export default Posts;
