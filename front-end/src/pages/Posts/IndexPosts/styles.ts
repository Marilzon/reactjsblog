import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
`;
export const PostContainer = styled.div`
  text-align: center;
  border: 1px solid #9FA3A6;
  width: 80vw;
  margin: 12px;
  padding: 12px;

  span {
    color: #A67C63;
  }
`;
