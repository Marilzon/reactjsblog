import styled from 'styled-components';

export const Button = styled.button`
padding: 6px;
margin-top: 12px;
border: 1.5px #5588 solid;
color: #5588;
border-radius: 2px;
text-decoration: none;
transition: 0.2s;

&:hover{
  background-color: #5599;
  color: #fff;
}
`;
