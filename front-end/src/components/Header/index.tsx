import React from 'react';

import { Container } from './styles';

interface HeaderProps {
  title: String;
}

const Header: React.FC<HeaderProps> = (props) => {
  return (
    <>
      <Container>{props.title}</Container>
    </>
  );
}

export default Header;
