import styled from 'styled-components';

export const Container = styled.header`
  font-size: 24px;
  text-align: center;
  color: #3a3a3a;
`;
